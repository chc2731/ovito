<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="viewport_layers.color_legend"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Color legend viewport layer</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/viewport_layers/color_legend_overlay_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>
    This <link linkend="viewport_layers">viewport layer</link> renders the color scale for a 
    <link linkend="particles.modifiers.color_coding">Color coding</link> modifier in the current pipeline 
    or the list of discrete element types and corresponding colors into the output picture. 
    The color legend helps observers to associate object colors in the picture with corresponding numeric values or element types. 
    The following figure shows two typical examples for color legends: A color map for a continuous particle property 
    and a discrete color legend for the <literal>Particle Type</literal> property:
    <screenshot><mediaobject><imageobject>
       <imagedata fileref="images/viewport_layers/color_legend_example.jpg" format="JPG" scale="95" />
    </imageobject></mediaobject></screenshot>
  </para>

  <simplesect>
    <title>Color source</title>

    <para>
      The <emphasis>Color source</emphasis> selection box at the top of the panel lets you choose where the legend should take 
      its colors from. In the current program version, two types of sources are supported:
      <variablelist>
        <varlistentry>
          <term>Color coding modifier</term>
          <listitem>
            <para>The legend can display the color gradient and the interval range of a <link linkend="particles.modifiers.color_coding">Color coding</link> modifier 
            that is part of the current data pipeline.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Typed properties</term>
          <listitem>
            <para>Alternatively, the legend can display the set of discrete element types defined for a property in the output of the 
            current data pipeline. Typical examples are the particle properties <literal>Particle Type</literal>, <literal>Structure Type</literal> or <literal>Residue Type</literal>. 
            The legend displays the name and the associated color of each type defined by the selected source property. 
            Note that the color legend layer itself is not responsible for coloring the particles or bonds. It may be necessary to insert a <link linkend="particles.modifiers.color_by_type">Color by type</link> 
            modifier into the pipeline. </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </para>

  </simplesect>

  <simplesect>
    <title>Parameters</title>

    <para>
      The other parameters of the color legend layer let you control the size, positioning and appareance of the color legend in the rendered picture.
      By default, the legend will be labeled with the name of the source property and, if based on a <link linkend="particles.modifiers.color_coding">Color coding</link> modifier, the 
      numeric interval range set for that modifier. You can override the text of these labels by entering something into the input fields <emphasis>Custom title</emphasis> and <emphasis>Custom label</emphasis>.
    </para>

    <para>
      If the legend is for a <link linkend="particles.modifiers.color_coding">Color coding</link> modifier,
      the number formatting of the min/max values is controlled by a format specification string. 
      You have the choice between decimal notation (<literal>%f</literal>), exponential notation (<literal>%e</literal>) and an automatic mode (<literal>%g</literal>), which picks the best representation depending on the value. 
      Furthermore, the format string gives you explicit control over the output precision, i.e. the number of digits that
      appear after the decimal point. Use <literal>"%.2f"</literal>, for example, to always show two digits after the decimal point. 
      The format string must follow the rules of the standard  
      <link xlink:href="https://en.cppreference.com/w/cpp/io/c/fprintf">printf()</link> C function.
      Additionally, it is possible to append a physical unit to the format string, e.g. <literal>"%g eV"</literal>, if desired. 
    </para>

  </simplesect>

  <simplesect>
    <title>See also</title>
    <para>
      <pydoc-link href="modules/ovito_vis" anchor="ovito.vis.ColorLegendOverlay"><classname>ColorLegendOverlay</classname> (Python API)</pydoc-link>
    </para>
  </simplesect>

</section>