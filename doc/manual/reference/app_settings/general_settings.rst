.. _application_settings.general:

General settings
================

.. image:: /images/app_settings/general_settings.*
  :width: 38%
  :align: right

This page of the :ref:`application settings dialog <application_settings>` lets you change several
global options that affect the user interface and the graphics system of the program. 

User interface
""""""""""""""

Load file: Use alternative file selection dialog
  This option controls the type of file selector that is shown each time you import a data file into OVITO. 
  By the default, the standard file dialog box of the operating system is used. If you set this option,
  a built-in dialog is used instead, which may or may not provide some advantages over the OS dialog box.

Modifiers list: Sort by category
  If this option is turned on, the :ref:`list of available modifiers <particles.modifiers>` 
  will be divided into several categories. If turned off, the modifiers will be shown as
  one contiguous, alphabetically ordered list.  
